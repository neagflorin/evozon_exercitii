from exec import intro


def test_upper_backwords():
    assert intro.upper_backwords("anuta") == "ATUNA"
    assert intro.upper_backwords("FLorin") == "NIROLF"


def test_is_palindrome():
    assert intro.is_palindrome(5) is True
    assert intro.is_palindrome(1551) is True
    assert intro.is_palindrome(523) is False
    assert intro.is_palindrome(-5) is False


def test_palindromes_under():
    assert intro.palindromes_under(10) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert intro.palindromes_under(50) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 22, 33, 44]
