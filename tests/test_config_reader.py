from exec import config_reader
import pytest


def test_config_reader():
    items = config_reader.load("tests/config.txt")
    assert len(items) == 15
    assert items['global.name'] == 'Florin'
    assert items['global.hobbies'] == ['playing games', 'walk in the woods', 100, 'driving', 1]
    assert items['sports.football'] == 10
    assert items['personal.cnp'] == 1960824125999
    assert items['personal.surname'] == 'Neag'
    assert items['computer.serial'] == '1-32-43 21-22-54'
    assert items["global.role"] == "developer"
    config_reader.dump("tests/parsed_config.txt", items)
    still_those_items = config_reader.load("tests/parsed_config.txt")
    assert items == still_those_items
    with pytest.raises(IOError):
        config_reader.load("abc")
