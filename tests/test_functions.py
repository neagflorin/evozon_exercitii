from exec import functions


def test_is_prime():
    assert functions.is_prime(1) is False
    assert functions.is_prime(2) is True
    assert functions.is_prime(3) is True
    assert functions.is_prime(42) is False
    assert functions.is_prime(23) is True


def test_primes_under():
    assert functions.primes_under(30) == [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
    assert functions.primes_under(10) == [2, 3, 5, 7]


def test_fibo_iterative():
    assert functions.fibo_iterative(8) == [1, 1, 2, 3, 5, 8, 13, 21]
    assert functions.fibo_iterative(12) == [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144]


def test_fibo_recursive():
    assert functions.fibo_recursive(0) == 1
    assert functions.fibo_recursive(1) == 1
    assert functions.fibo_recursive(5) == 8
    assert functions.fibo_recursive(10) == 89


def test_map_functionality():
    assert functions.map_functionality(lambda a: a + 2, [1, 2, 3]) == [3, 4, 5]
    assert functions.map_functionality(lambda a: a + 2, [3, 1, 7]) == [5, 3, 9]


def test_filter_functionality():
    assert functions.filter_functionality(lambda a: a % 2 == 0, [1, 2, 3, 4, 5, 6]) == [2, 4, 6]
    assert functions.filter_functionality(lambda a: a % 2 == 0, [1, 3]) == []
    assert functions.filter_functionality(lambda a: a % 2 == 0, [2, 4, 6]) == [2, 4, 6]


def test_reduce_functionality():
    assert functions.reduce_functionality(lambda a, b: a + b, [1, 2, 3, 4, 5], 2) == 17
    assert functions.reduce_functionality(lambda a, b: a + b, [1, 2, 3, 4, 5]) == 15
    assert functions.reduce_functionality(lambda a, b: a + b, [1, 2, 3, 4, 5], 4) == 19


def test_sum_under_with_constraint():
    assert functions.sum_under_with_constraint(10) == 27
    assert functions.sum_under_with_constraint(3) == 3
    assert functions.sum_under_with_constraint(7) == 12


def test_binary_search():
    assert functions.binary_search([1, 2, 3, 4, 5, 6, 7, 8], 6) is True
    assert functions.binary_search([1, 2, 3, 4, 5, 6, 7, 8], 3) is True
    assert functions.binary_search([1, 2, 3, 4, 5, 6, 7, 8], 1) is True
    assert functions.binary_search([1, 2, 3, 4, 5, 6, 7, 8], 8) is True
    assert functions.binary_search([1, 2, 3, 4, 5, 6, 7, 8], -1) is False
    assert functions.binary_search([1, 2, 3, 4, 5, 6, 7, 8], 10) is False


def test_successive_values_returned():
    assert functions.successive_values_returned() == functions.successive() == 0
    assert functions.successive_values_returned() == functions.successive() == 1
    assert functions.successive_values_returned() == functions.successive() == 2
    assert functions.successive_values_returned() == functions.successive() == 3
