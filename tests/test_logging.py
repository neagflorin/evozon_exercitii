from exec import logging


def test_logging():
    open("tests/log.txt", "w").close()
    log = logging.Logger(file="tests/log.txt")
    log.set_mode(logging.CONSOLE_AND_FILE_MODE)
    log.info("nu se afiseaza")
    log.warning("se afiseaza")
    log.error("si asta se afiseaza")
    log.debug("asta nu")
    log.critical("asta da")

    log.set_mode("console")
    log.set_level(logging.DEBUG_LEVEL)
    log.info("se afiseaza DOAR in consola")
    log.debug("se afiseaza si asta DOAR in consola")

    log.set_mode(logging.FILE_MODE)
    log.warning("se afiseaza DOAR in fisier")
    log.error("si asta se afiseaza DOAR in fisier")
