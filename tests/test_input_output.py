from exec import input_output, functions
from io import StringIO

numbers = StringIO("\n".join(map(str, [1, 2, 3, 5, 7, 10, 13, 23, 33, 44, 67, 89])))
other_numbers = StringIO("\n".join(map(str, [1, 10, 100, 1000])))
another_numbers = StringIO("\n".join(map(str, [23, 45, 111, 123, 345, 3476])))
text = StringIO("""ana are mere
ana are mul3te mere
cineva nu are mere ci are pere23 11 a12""")


def test_sum_of_numbers():
    assert input_output.sum_of_numbers(numbers) == 297
    assert input_output.sum_of_numbers(other_numbers) == 1111


def test_all_primes_under():
    prime_numbers = StringIO()
    input_output.all_primes_under(50, prime_numbers)
    primes = functions.primes_under(50)
    for i, line in enumerate(prime_numbers):
        assert primes[i] == int(line)


def test_one_time_words():
    words = input_output.one_time_words(text)
    assert words == ['mul3te', 'cineva', 'nu', 'ci', 'pere23', 'a12']


def test_sorted_numbers():
    all_sorted = StringIO()
    input_output.sorted_numbers(all_sorted, numbers, other_numbers, another_numbers)

    expected_output_values = [1, 1, 2, 3, 5, 7, 10, 10, 13, 23, 23, 33, 44, 45, 67, 89, 100, 111, 123, 345, 1000, 3476]

    for i, line in enumerate(all_sorted):
        assert expected_output_values[i] == int(line)

    input_output.sorted_numbers(all_sorted, numbers, other_numbers)

    expected_output_values = [1, 1, 2, 3, 5, 7, 10, 10, 13, 23, 33, 44, 67, 89, 100, 1000]

    for i, line in enumerate(all_sorted):
        assert expected_output_values[i] == int(line)


def test_all_links_from_website():
    links = input_output.all_links_from_website("https://docs.google.com/document/d/1mdaDoZ4TIuTevoD8z-"
                                                "DLna9X3WQBa2uqpcF-1GNJllw/edit#")
    assert len(links) == 18

