# 1


def upper_backwords(name):
    return name[::-1].upper()


# 2


def is_palindrome(number):
    if number < 0:
        return False
    else:
        return number == int(upper_backwords(str(number)))

# 3


def palindromes_under(number):
    return [nr for nr in range(number) if is_palindrome(nr)]

