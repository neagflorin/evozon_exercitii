from time import time, sleep
import functools


# 11


def execution_time_decorator(func):
    print('am decorat {}'.format(func.__name__))

    @functools.wraps(func)
    def func_wrapper(*args, **kwargs):
        start_time = time()
        return_value = func(*args, **kwargs)
        final_time = time()
        print("function was executed in {} seconds".format(final_time - start_time))
        return return_value

    return func_wrapper


# 12


class TimeCounter:
    def __init__(self):
        self.time = 0


def total_time(func, t=TimeCounter()):

    @functools.wraps(func)
    def func_wrapper(*args, **kwargs):
        start_time = time()
        return_value = func(*args, **kwargs)
        final_time = time() - start_time
        t.time += final_time
        print("total execution time of decorated functions is {}".format(t.time))
        return return_value

    return func_wrapper


@total_time
@execution_time_decorator
def t1():
    sleep(1)


@total_time
def t2():
    sleep(1)

t1()
t2()
t1()


# 13


def memoization_decorator(func):
    memories = {}

    def func_wrapper(*args):
        if args not in memories:
            memories[args] = func(*args)
        return memories[args]

    return func_wrapper


# 1


def is_prime(number):
    if number < 2:
        return False
    for d in range(2, number // 2 + 1):
        if number % d == 0:
            return False
    return True


# 2


def primes_under(number):
    return [nr for nr in range(number) if is_prime(nr)]


# 3


@total_time
@execution_time_decorator
def fibo_iterative(n):
    a, b = 1, 1
    fibos = [a, b]
    for i in range(n-2):
        a, b = b, a+b
        fibos.append(b)
    return fibos


@total_time
@memoization_decorator
def fibo_recursive(n):
    if n == 0 or n == 1:
        return 1
    return fibo_recursive(n - 1) + fibo_recursive(n - 2)


# 4


def map_functionality(func, iterable):
    return [func(elem) for elem in iterable]


# 5


def filter_functionality(func, iterable):
    return [elem for elem in iterable if func(elem)]


# 6


def reduce_functionality(func, iterable, initializer=0):
    result = initializer
    for elem in iterable:
        result = func(result, elem)
    return result


# 7


def sum_under_with_constraint(number):
    # constraint: n * n - 1 divisible by 3
    mapped = zip(range(1, number), map_functionality(lambda x: x * x - 1, range(1, number)))
    filtered = filter_functionality(lambda x: x[1] % 3 == 0, mapped)
    sum_ = reduce_functionality(lambda a, b: a + b[0], filtered)
    return sum_


# 8


def sort_list(list_):
    for i in range(len(list_) - 1):
        for j in range(i, len(list_)):
            if list_[i] > list_[j]:
                list_[i], list_[j] = list_[j], list_[i]
    return list_


# 9


@total_time
@execution_time_decorator
def binary_search(sorted_list, elem):
    left = 0
    right = len(sorted_list) - 1
    while left <= right:
        middle = (left + right) // 2
        if elem == sorted_list[middle]:
            return True
        elif elem < sorted_list[middle]:
            right = middle - 1
        else:
            left = middle + 1
    return False


# 10


def successive_values_returned():
    if not hasattr(successive_values_returned, "count"):
        successive_values_returned.count = 0
    else:
        successive_values_returned.count += 1
    return successive_values_returned.count


class Counter:
    def __init__(self):
        self.count = -1


def successive(_k=Counter()):
    _k.count += 1
    return _k.count
