from exec import functions
from urllib.request import urlopen
from bs4 import BeautifulSoup
import re
from io import StringIO


# 1


def sum_of_numbers(filename):
    sum_ = 0

    if isinstance(filename, str):
        with open(filename) as f:
            for line in f:
                sum_ += int(line)
    elif isinstance(filename, StringIO):
        for line in filename:
            sum_ += int(line)

    return sum_


# 2


def all_primes_under(number, filename):
    numbers = functions.primes_under(number)
    str_numbers = "\n".join(functions.map_functionality(str, numbers))
    if isinstance(filename, str):
        with open(filename, "w") as f:
            f.write(str_numbers)
    elif isinstance(filename, StringIO):
        filename.write(str_numbers)

# 3


def one_time_words(filename):
    words_counter = {}
    if isinstance(filename, str):
        with open(filename) as f:
            for line in f:
                words = line.split()
                for word in words:
                    if word.isalnum() and not word.isdigit():
                        if word in words_counter:
                            words_counter[word] += 1
                        else:
                            words_counter[word] = 1
    elif isinstance(filename, StringIO):
        for line in filename:
            words = line.split()
            for word in words:
                if word.isalnum() and not word.isdigit():
                    if word in words_counter:
                        words_counter[word] += 1
                    else:
                        words_counter[word] = 1

    words_appearing_one_time = []
    for word, frequency in words_counter.items():
        if frequency == 1:
            words_appearing_one_time.append(word)

    return words_appearing_one_time


# 4


def merge(list1, list2):
    result = []
    i = j = 0
    while i < len(list1) and j < len(list2):
        if list1[i] < list2[j]:
            result.append(list1[i])
            i += 1
        else:
            result.append(list2[j])
            j += 1
    # if i < len(list1):
    result.extend(list1[i:])
    # elif j < len(list2):
    result.extend(list2[j:])

    return result


def sorted_numbers(destination_file, *files):
    numbers = []
    for index, file in enumerate(files):
        numbers.append([])
        if isinstance(file, str):
            with open(file) as f:
                for line in f:
                    numbers[index].append(int(line))
        elif isinstance(file, StringIO):
            for line in file:
                numbers[index].append(int(line))
    sorted_list1 = numbers[0]
    for sorted_list2 in numbers[1:]:
        sorted_list1 = merge(sorted_list1, sorted_list2)
    str_numbers = "\n".join(functions.map_functionality(str, sorted_list1))
    if isinstance(destination_file, str):
        with open(destination_file, "w") as f:
            f.write(str_numbers)
    elif isinstance(destination_file, StringIO):
        destination_file.write(str_numbers)


# 5


def all_links_from_website(url):
    soup = BeautifulSoup(urlopen(url), 'html.parser')
    links = set()
    for matches in re.findall("https://.*\..*", soup.decode("utf-8")):
        link = matches.split()[0]
        links.add(link)

    return links
