DEBUG_LEVEL, INFO_LEVEL, WARNING_LEVEL, ERROR_LEVEL, CRITICAL_LEVEL = [10 * i for i in range(1, 6)]

CONSOLE_MODE, FILE_MODE, CONSOLE_AND_FILE_MODE = "console", "file", "console and file"


class Logger:
    levels = {DEBUG_LEVEL,
              INFO_LEVEL,
              WARNING_LEVEL,
              ERROR_LEVEL,
              CRITICAL_LEVEL}

    modes = {CONSOLE_MODE,
             FILE_MODE,
             CONSOLE_AND_FILE_MODE}

    def __init__(self, mode=CONSOLE_MODE, level=WARNING_LEVEL, file=None):
        self.file = file
        if file is not None:
            self.ready_file = open(self.file, "a")

        self.set_level(level)
        self.set_mode(mode)

    def set_level(self, level):
        if level in self.levels:
            self.level = level
        else:
            raise ValueError("invalid level")

    def set_mode(self, mode):
        if mode in self.modes:
            self.mode = mode
        else:
            raise ValueError("invalid mode")

    def set_file(self, file):
        self.file = file

    def handle_log_message(self, level, message):
        if self.level <= level:
            if self.file is None and self.mode != CONSOLE_MODE:
                raise ValueError("file not specified")

            if self.mode == CONSOLE_MODE or self.mode == CONSOLE_AND_FILE_MODE:
                print(message)

            if self.mode == FILE_MODE or self.mode == CONSOLE_AND_FILE_MODE:
                self.ready_file.write(message + "\n")

    def debug(self, message):
        self.handle_log_message(DEBUG_LEVEL, "DEBUG : " + message)

    def info(self, message):
        self.handle_log_message(INFO_LEVEL, "INFO : " + message)

    def warning(self, message):
        self.handle_log_message(WARNING_LEVEL, "WARNING : " + message)

    def error(self, message):
        self.handle_log_message(ERROR_LEVEL, "ERROR : " + message)

    def critical(self, message):
        self.handle_log_message(CRITICAL_LEVEL, "CRITICAL : " + message)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.ready_file.close()
