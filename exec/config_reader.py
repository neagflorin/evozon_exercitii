def load(filename):
    dict_ = dict()
    try:
        with open(filename) as f:
            remember_section = "global"
            for line in f:
                if line == "\n" or line.startswith("#"):
                    continue
                if line.startswith("["):
                    if line.endswith("]\n") and line[1:-2].isalpha():
                        remember_section = line[1:-2]
                    else:
                        raise ValueError("invalid format at line: {}".format(line))
                else:
                    items = line.split()
                    if items[0].isalpha() and items[1] == "=":
                        key = remember_section + "." + items[0]
                    else:
                        raise ValueError("invalid format at line: {}".format(line))
                    if items[2][0] == "'" or items[2][0] == '"':
                        if items[-1][-1] == items[2][0]:
                            value = " ".join(items[2:])[1:-1]
                        else:
                            raise ValueError("invalid format at line: {}".format(line))
                    elif items[2].isdigit() and len(items) == 3:
                        value = int(items[2])
                    elif items[2].startswith("["):
                        value = []
                        items = " ".join(items[2:])[1:-1]
                        items = items.split(",")
                        items = map(str.strip, items)
                        for item in items:
                            if item.isdigit():
                                item = int(item)
                            value.append(item)
                    else:
                        value = " ".join(items[2:])
                    dict_[key] = value
        return dict_
    except IOError:
        raise IOError("file not found")


def dump(config_filename, items):
    sections = set()
    write_global = False
    try:
        with open(config_filename, "w") as f:
            for k, v in items.items():
                section, key = k.split(".")
                if section == "global" and write_global:
                    f.write("[global]\n")
                    write_global = False
                if section != "global" and section not in sections:
                    f.write("[" + section + "]\n")
                    sections.add(section)
                    write_global = True
                f.write(key + " = ")
                if isinstance(v, str):
                    if len(v.split()) == 1:
                        f.write(v + "\n")
                    else:
                        f.write("'" + v + "'\n")
                elif isinstance(v, int):
                    f.write(str(v) + "\n")
                else:
                    f.write("[")
                    for elem in v[:-1]:
                        f.write(str(elem) + ", ")
                    f.write(str(v[-1]) + "]\n")
    except IOError:
        raise IOError("file not found")
